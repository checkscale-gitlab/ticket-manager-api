import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: 'ticket'})
export class TicketEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column("text")
  description: string;

  @Column("text")
  name: string;

  // to do / done / warning ...
  @Column("integer")
  status: number;

  // position on the board ?
  @Column("integer")
  index: number;
}