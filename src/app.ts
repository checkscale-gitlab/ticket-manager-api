import 'reflect-metadata';
import * as express from 'express';
import { Express } from "express";
import { createConnection } from "typeorm";
import { TicketEntity } from "./entities/Ticket";

require ('dotenv').config();

// CONNECTION TO DATABASE USING TYPEORM
createConnection({
  type: <'mariadb'|'mysql'>process.env.DB_TYPE,
  url: <string>process.env.ORM_URL,
  entities: [__dirname+'/entities/*.ts'],
  synchronize: Boolean(true),
  logging: Boolean(false)
}).then(connection => {
  console.log('Successfully connected to DB');
  const ticketRepository = connection.getRepository(TicketEntity);

  // CREATE AND SETUP EXPRESS APP
  const app: Express = express();

  // REGISTER ROUTES
  app.get('/tickets', async (req, res) => {
    const tickets = await ticketRepository.find();

    if (tickets) { return res.status(200).json(tickets); }
    else { return res.status(404).send(); }
  });

  app.get('/tickets/:id', async (req, res) => {
    const ticket = await ticketRepository.findOne(req.params.id);

    if (ticket) { return res.status(200).send(ticket); }
    else { return res.status(404).send(); }
  });

  app.post('/tickets', async (req, res) => {
    try {
      const ticket = await ticketRepository.create(req.body);
      const results = await ticketRepository.save(ticket);
      return res.status(200).send();
    } catch {
      return res.status(400).send();
    }
  });

  app.put('/tickets/:id', async (req, res) => {
    try {
      const ticket = await ticketRepository.findOne(req.params.id);
      ticketRepository.merge(ticket, req.body);
      const results = await ticketRepository.save(ticket);
      return res.status(200).send(results);
    } catch {
      return res.status(400).send();
    }
  });

  app.delete('/tickets/:id', async (req, res) => {
    const ticket = await ticketRepository.findOne(req.params.id);
    if (ticket) {
      const tickets = await ticketRepository.delete(req.params.id);
      return res.status(200).send(tickets);
    } else { return res.status(404).send(); }
  });

  // START EXPRESS SERVER
  app.listen(8000, () => {
    console.log('API available at http://localhost:%s/tickets', process.env.NODE_PORT)
    console.log('phpMyAdmin available at http://localhost:%s', process.env.PMA_PORT)
    console.log(`phpMyAdmin ID username: %s`, process.env.DB_NAME)
    console.log(`phpMyAdmin ID password: %s`, process.env.DB_PASSWORD)

  });
}).catch(error => console.log('Error connecting to DB', error));